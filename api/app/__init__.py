from flask import Flask, request
import json

from flask.wrappers import Response
from flask_cors import CORS
from .database import db, migrate
from .database.post import Comment, Post

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://postgres:postgres@db'

cors = CORS()

cors.init_app(app)
db.init_app(app)
migrate.init_app(app, db)

@app.route("/api/v1/post/", methods=["GET", "POST"])
def post_list_or_create():
    if request.method == "GET":
        return Response(json.dumps([post.serialize() for post in Post.query.order_by(Post.id.desc()).all()]))

    if request.method == "POST":
        post = Post(title="Enter a title here.", text="Enter blog post body here.")
        db.session.add(post)
        db.session.commit()
        return Response(json.dumps(post.serialize()), 200)

    raise Exception("Invalid request method.")

@app.route("/api/v1/post/<id>/", methods=["GET", "DELETE", "PUT"])
def post_get_or_delete(id):
    if request.method == "GET":
        return Response(json.dumps(Post.query.get_or_404(id).serialize()), 200)
    
    if request.method == "DELETE":
        post = Post.query.get_or_404(id)
        db.session.delete(post)
        db.session.commit()
        return Response("{}", 200)
    
    if request.method == "PUT":
        post = Post.query.get_or_404(id)
        post.deserialize(request.json)
        db.session.commit()
        
        return Response(json.dumps(post.serialize()), 200)

    raise Exception("Invalid request method.")

@app.route("/api/v1/post/<id>/comment/", methods=["POST"])
def post_comment_create(id):
    if request.method == "POST":
        comment = Comment(text=request.json["text"], post_id=id)
        db.session.add(comment)
        db.session.commit()
        return Response(json.dumps(comment.serialize()), 200)

    raise Exception("Invalid request method.")