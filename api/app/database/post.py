from app.database.comment import Comment
from . import db

class Post(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(80), unique=False, nullable=False)
    text = db.Column(db.Text(), unique=False, nullable=False)
    
    def serialize(self):
        return {
            "id": self.id,
            "title": self.title,
            "text": self.text,
            "comments": [comment.serialize() for comment in self.comments()]
        }
    
    def deserialize(self, data):
        if "id" in data:
            self.title = data["title"]
            self.text = data["text"]
        else:
            post = Post(**data)
            db.session.add(post)
        
        db.session.commit()
        
    def comments(self):
        return Comment.query.filter_by(post_id=self.id).all()