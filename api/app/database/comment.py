from . import db

class Comment(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    text = db.Column(db.Text(), unique=False, nullable=False)

    post_id = db.Column(db.Integer, db.ForeignKey('post.id', ondelete="CASCADE"))
    post = db.relationship('Post', foreign_keys=post_id)

    def serialize(self):
        return {
            "id": self.id,
            "text": self.text,
            "post_id": self.post_id
        }
    
    @staticmethod
    def deserialize(self, data):
        return Comment(text=data["text"])