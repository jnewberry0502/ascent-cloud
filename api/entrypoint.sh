#!/bin/bash

# Delay until database has fully spun up

until PGPASSWORD=postgres psql -h "db" -U "postgres" -c '\q'; do
  >&2 echo "Postgres is unavailable - sleeping"
  sleep 1
done

flask db init
flask db migrate
flask db upgrade

exec "$@"