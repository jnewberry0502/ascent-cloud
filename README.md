# Ascent Cloud Blog
This repo contains the code for a simple web blog, as part of the AscentCloud's application process.

# Setup instructions:

## Recommended installation: using Docker

Using docker, you can automatically spin up all the resources and requirements necessary for running the application.

Docker Desktop (Mac and Windows): https://www.docker.com/products/docker-desktop \
Docker Engine (Linux): https://docs.docker.com/engine/install/#server (May have to install docker-compose seperately on Linux)

Clone this repo, and then run the commands below
> docker-compose build \
> docker-compose up -d

This should build the containers for the "web" and "api" containers, and then start development servers for both applications.

## Alternative installation: Manual installation

Install [python](https://www.python.org/downloads/), [nodejs](https://nodejs.org/en/), and [PostgreSQL](https://www.postgresql.org/download/) on your computer.

Clone this repo, and run the following commands

Frontend:
> cd web \
> npm install \
> npm run start

Backend: \
Change the URL in api/app/__init__.py to be the URL for your PostgreSQL database (postgres:postgres@localhost) \
> cd api \
> pip install -r requirements.txt \
> flask run

Database: \
Follow the setup instructions, setting the username and password to "postgres" \

## Post Installation

Then, the following links should point to the application \
[http://localhost:8080/](http://localhost:8080/) - React Frontend
 - / - Homepage that contains the most recent blog posts
 - /post/\<id\> - Page that contains the content for a blog post
 - /post/\<id\>/edit - Page where you can edit a blog post

[http://localhost:8081/](http://localhost:8081/) - Flask Backend
 - /api/v1/post/ - JSON response with a list of posts, with the most recent ones appearing first
 - /api/v1/post/\<id\>/ - JSON response with the post information

## Development Instructions

If using VSCode, you can use the [Remote-Containers](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.remote-containers) extension to open each folder in it's corresponding docker container. Simply install the extension, load each application's folder in a separate window, and type "Reopen in Container" in the command menu in VSCode. This should automatically rebuild the container and open it, with the required extensions.

## Technologies Used
### Frontend
- React
- React-Router
- React-Bootstrap and React-Bootstrap-Icons

### Backend
- Flask
- Flask-Sqlalchemy with PostgreSQL interface
- Flask-Migrate for database migrations

### Database
- PostgreSQL (through a docker container)