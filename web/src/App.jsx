import React from "react"
import { Container } from "react-bootstrap"
import { Route, BrowserRouter as Router, Switch } from "react-router-dom"
import "./App.css"
import BlogPost from "./BlogPost"
import BlogPostEditor from "./BlogPostEditor"
import Home from "./Home"

export default class App extends React.Component {
    render(){
        return <Router>
            <Container className="mt-2">

            <Switch>
                <Route exact path="/">
                <Home />
                </Route>
                <Route exact path="/post/:id/" >
                <BlogPost />
                </Route>
                <Route exact path="/post/:id/edit/">
                <BlogPostEditor />
                </Route>
            </Switch>
          </Container>
      </Router>
    }
}