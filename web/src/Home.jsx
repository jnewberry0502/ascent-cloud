import React from 'react'
import { withRouter } from 'react-router'

import { default as blogPostAPI } from "./BlogPostAPI"
import BlogPostPreview from './BlogPostPreview'

import { ListGroup, Row, Col, Card, Button } from "react-bootstrap"

class Home extends React.Component {
    state = {
        posts: []
    }

    update() {
        blogPostAPI.list().then(data => this.setState({
            posts: data
        }))
    }

    componentDidMount() {
        this.update();
    }

    onCreateButtonClick() {
        blogPostAPI.create().then(data =>
            this.props.history.push(`/post/${data.id}/edit/`))
    }

    render() {
        return (
            <Row className="justify-content-md-center">
                <Col md="8">
                    <Card>
                        <Card.Header>
                            <Card.Title>Recent Posts</Card.Title>
                        </Card.Header>
                        <Card.Body>
                            <Card.Text>
                                <ListGroup>
                                    {this.state.posts.map(post => <BlogPostPreview post={post}></BlogPostPreview>)}
                                </ListGroup>
                            </Card.Text>

                            <Button variant="primary" onClick={this.onCreateButtonClick.bind(this)}>Create a new post</Button>

                        </Card.Body>
                    </Card>
                </Col>
            </Row>
        )
    }
}

export default withRouter(Home);