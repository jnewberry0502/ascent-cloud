import React from "react"
import { Button, Card, Col, Form, InputGroup, ListGroup, Row } from "react-bootstrap";
import { withRouter } from "react-router";
import { Link } from "react-router-dom"
import "./BlogPost.css"

import { ArrowLeft } from 'react-bootstrap-icons';

import { default as blogPostAPI } from "./BlogPostAPI"
import BlogPostBase from "./BlogPostBase";

export class BlogPost extends BlogPostBase {
    state = {
        ...this.state,
        newComment: ""
    }

    get id() {
        return this.props.match.params.id;
    }

    postCommentClick() {
        let commentAPI = blogPostAPI.getCommentAPI(this.id);
        commentAPI.post({
            text: this.state.newComment
        }).then(data => {
            this.setState({
                newComment: ""
            })
            return data
        }).then(data => this.update())
    }

    render() {
        return <Row className="justify-content-md-center">
            <Link to="/"><Button className="my-1"><ArrowLeft></ArrowLeft> View All Posts</Button></Link>

            <Col md="8">
                <Card>
                    <Card.Header>
                        <Card.Title>
                            {this.state.title}
                        </Card.Title>
                    </Card.Header>
                    <Card.Body>
                        <Card.Text>
                            {this.state.text}
                        </Card.Text>
                        <Link to={`/post/${this.id}/edit`}><Button>Edit</Button></Link>
                    </Card.Body>

                    <Card.Body>
                        <Card.Subtitle className="mb-1">
                            Comments
                        </Card.Subtitle>
                        <Card.Text>
                            <ListGroup variant="flush">
                                {this.state.comments.map(comment =>
                                    <ListGroup.Item>{comment.text}</ListGroup.Item>
                                )}
                            </ListGroup>

                            <Form>

                                <InputGroup>
                                    <Form.Control type="text" onChange={(e) => this.setState({ newComment: e.target.value })} value={this.state.newComment} placeholder="Enter your comment here" />
                                    <Button onClick={this.postCommentClick.bind(this)}>Post</Button>
                                </InputGroup>
                            </Form>

                        </Card.Text>
                    </Card.Body>
                </Card>


            </Col>
        </Row>
    }
}

export default withRouter(BlogPost)