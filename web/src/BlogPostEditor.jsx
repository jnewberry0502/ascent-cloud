import React from "react"
import { Button, ButtonGroup, Form, Card, Row, Col } from "react-bootstrap"
import { withRouter } from "react-router"
import "./BlogPost.css"

import { default as blogPostAPI } from "./BlogPostAPI"
import BlogPostBase from "./BlogPostBase"
import { ArrowLeft } from 'react-bootstrap-icons';
import {Link} from "react-router-dom";

class BlogPostEditor extends BlogPostBase {
    get id() {
        return this.props.match.params.id;
    }

    saveButtonClick() {
        blogPostAPI.update(this.id, {
            id: this.id,
            title: this.state.title,
            text: this.state.text
        }).then(data => this.props.history.push(`/post/${this.id}/`))
    }

    deleteButtonClick() {
        blogPostAPI.delete(this.id).then(data => this.props.history.push("/"))
    }

    render() {
        return <Row className="justify-content-md-center">
            <Link to={`/post/${this.id}`}><Button className="my-1"><ArrowLeft></ArrowLeft> View Post</Button></Link>
            <Col md="8">
                <Card>
                    <Card.Header>
                        <Card.Title>Post Editor</Card.Title>
                    </Card.Header>
                    <Card.Body>
                        <Card.Text>
                            <Form>
                                <Form.Group className="mb-1">
                                    <Form.Label>Title</Form.Label>
                                    <Form.Control type="text" onChange={(e) => this.setState({ title: e.target.value })} value={this.state.title} />
                                </Form.Group>

                                <Form.Group className="mb-1">
                                    <Form.Label>Post Body</Form.Label>
                                    <Form.Control as="textarea" rows="5" onChange={(e) => this.setState({ text: e.target.value })} value={this.state.text} />
                                </Form.Group>

                                <ButtonGroup>
                                    <Button variant="danger" onClick={this.deleteButtonClick.bind(this)}>Delete</Button>
                                    <Button variant="primary" onClick={this.saveButtonClick.bind(this)}>Save</Button>
                                </ButtonGroup>
                            </Form>
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col></Row>

    }
}

export default withRouter(BlogPostEditor)