import React from 'react'
import {default as blogPostAPI} from "./BlogPostAPI"

class BlogPostBase extends React.Component {
    state = {
        title: null,
        text: null,
        comments: []
    }

    update(){
        blogPostAPI.get(this.id).then(data => {
            this.setState({
                title: data.title,
                text: data.text,
                comments: data.comments
            })
        }).catch(e => {
            this.props.history.push("/")
        })
    }

    componentDidMount(){
        this.update();
    }

    postCommentClick(pk) {
        let comment_api = blogPostAPI.comment_api(pk);
        comment_api.post({
            text: this.state.new_comment
        }).then(this.update())
    }
}

export default BlogPostBase;