// could be retrieved from an environment variable
let baseURL = "http://localhost:8081/api/v1";

class BlogPostCommentAPI {
    constructor(postId){
        this.postId = postId;
    }

    post(data){
        return fetch(`${baseURL}/post/${this.postId}/comment/`, {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(response => response.json())
    }
}

class BlogPostAPI {
    get(id){
        return fetch(`${baseURL}/post/${id}/`).then(response => response.json())
    }

    delete(id){
        return fetch(`${baseURL}/post/${id}/`, {
            method: 'DELETE'
        })
    }

    update(id, data){
        return fetch(`${baseURL}/post/${id}/`, {
            method: 'PUT',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(response => response.json())
    }

    getCommentAPI(id){
        return new BlogPostCommentAPI(id);
    }

    list() {
        return fetch(`${baseURL}/post/`).then(response => response.json())
    }

    create() {
        return fetch(`${baseURL}/post/`, {
            "method": "POST",
            "data": {},
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(response => response.json())
    }
}

export default new BlogPostAPI();