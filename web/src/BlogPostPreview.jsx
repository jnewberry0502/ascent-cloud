import React from 'react'

import {ListGroup} from "react-bootstrap"
import {Link} from "react-router-dom"

export default class BlogPostPreview extends React.Component {
    render(){
        return <Link to={`/post/${this.props.post.id}`}>
            <ListGroup.Item>
                {this.props.post.title}
        </ListGroup.Item>
        </Link>
    }
}
