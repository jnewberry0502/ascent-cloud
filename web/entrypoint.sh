#!/bin/bash

rm -r /code/node_modules
ln -s /usr/src/cache/node_modules/ /code/node_modules

exec "$@"